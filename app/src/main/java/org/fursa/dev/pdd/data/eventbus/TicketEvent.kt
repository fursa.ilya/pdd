package org.fursa.dev.pdd.data.eventbus

data class TicketEvent(val number: Int)