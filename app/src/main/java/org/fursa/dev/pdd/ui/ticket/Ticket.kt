package org.fursa.dev.pdd.ui.ticket

data class Ticket(val number: Int, var progress: Int)