package org.fursa.dev.pdd

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_main.*
import org.fursa.dev.pdd.data.eventbus.HideEvent
import org.fursa.dev.pdd.data.eventbus.TicketEvent
import org.fursa.dev.pdd.ui.quiz.QuizFragment
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupNavigation()

        bottomNavigationView.setOnNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.item_tickets -> navController.navigate(R.id.fragment_tickets)
                else -> navController.navigate(R.id.fragment_settings)
            }

            return@setOnNavigationItemSelectedListener true
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this@MainActivity)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this@MainActivity)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        EventBus.getDefault().post(HideEvent(false))
    }

    private fun setupNavigation() {
        navController = findNavController(R.id.nav_host_fragment)
        bottomNavigationView.setupWithNavController(navController)
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onNavigationViewVisibleEvent(event: HideEvent) {
       if(event.hided) {
           bottomNavigationView.visibility = View.GONE
       } else {
           bottomNavigationView.visibility = View.VISIBLE
       }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onTicketSelectedEvent(event: TicketEvent) {
        val args = bundleOf("ticket_number" to event.number)
        navController.navigate(R.id.details_fragment, args)
    }
}
