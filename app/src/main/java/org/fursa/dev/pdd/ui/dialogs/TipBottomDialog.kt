package org.fursa.dev.pdd.ui.dialogs

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.tip_dialog_fragment.*
import org.fursa.dev.pdd.R
import org.fursa.dev.pdd.data.common.AppConst.TIP_BODY
import org.fursa.dev.pdd.data.common.AppConst.TIP_HEADER

class TipBottomDialog : BottomSheetDialogFragment() {

    override fun getTheme(): Int {
         return R.style.BottomSheetDialogTheme
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return BottomSheetDialog(requireContext(), theme)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.tip_dialog_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txtTitle.text = arguments!!.getString(TIP_HEADER)
        txtTip.text = arguments!!.getString(TIP_BODY)
    }

    companion object {

        fun getInstance(title: String, tip: String): TipBottomDialog {
            val dialog = TipBottomDialog()
            val args = Bundle().apply {
                putString(TIP_HEADER, tip)
                putString(TIP_BODY, title)
            }
            dialog.arguments = args
            return dialog
        }
    }
}