package org.fursa.dev.pdd.ui.ticket

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.ticket_row.view.*
import org.fursa.dev.pdd.R
import org.fursa.dev.pdd.data.common.AppConst.TICKET_NUMBER_KEY
import org.fursa.dev.pdd.data.eventbus.HideEvent
import org.fursa.dev.pdd.data.eventbus.TicketEvent
import org.greenrobot.eventbus.EventBus

class TicketAdapter : RecyclerView.Adapter<TicketAdapter.TicketViewHolder>() {
    private val tickets = mutableListOf<Ticket>()

    fun setTickets(tickets: MutableList<Ticket>) {
        this.tickets.clear()
        this.tickets.addAll(tickets)
        notifyDataSetChanged()
    }

    fun setProgress(ticketNumber: Int, progressValue: Int) {
        val ticket = tickets[ticketNumber]
        ticket.progress = progressValue
        tickets[ticketNumber] = ticket
        notifyItemChanged(ticketNumber)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TicketViewHolder {
        val ticketView = LayoutInflater.from(parent.context).inflate(R.layout.ticket_row, parent, false)
        return TicketViewHolder(ticketView)
    }

    override fun getItemCount() = tickets.count()

    override fun onBindViewHolder(holder: TicketViewHolder, position: Int) {
        val ticket = tickets[position]
        holder.bind(ticket)
    }

    inner class TicketViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(ticket: Ticket) = with(itemView) {
            txtTitle.text = "${resources.getString(R.string.ticket_label)} ${ticket.number}"
            txtProgress.text = "${ticket.progress}/20"
            progressBar.progress = ticket.progress
            itemView.setOnClickListener {
               val args = bundleOf(TICKET_NUMBER_KEY to ticket.number)
               itemView.findNavController().navigate(R.id.details_fragment, args)
               EventBus.getDefault().post(HideEvent(true))
               EventBus.getDefault().post(TicketEvent(ticket.number))
            }
        }
    }

}