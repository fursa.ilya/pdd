package org.fursa.dev.pdd.di

import dagger.Component
import org.fursa.dev.pdd.AppDelegate
import org.fursa.dev.pdd.ui.quiz.QuizViewModel
import org.fursa.dev.pdd.ui.ticket.TicketsViewModel
import javax.inject.Singleton

@Singleton
@Component(modules = [DatabaseModule::class])
interface AppComponent {
    fun inject(viewModel: QuizViewModel)
    fun inject(viewModel: TicketsViewModel)
    fun inject(appDelegate: AppDelegate)
}