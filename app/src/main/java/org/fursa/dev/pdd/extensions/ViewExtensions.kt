package org.fursa.dev.pdd.extensions

import android.view.View
import android.widget.Button
import android.widget.ImageView
import com.squareup.picasso.Picasso

fun ImageView.loadImg(url: String) {
    Picasso
        .get()
        .load(url)
        .into(this)
}

fun ImageView.visible() {
    this.visibility = View.VISIBLE
}

fun ImageView.gone() {
    this.visibility = View.GONE
}

fun Button.visible() {
    this.visibility = View.VISIBLE
}

fun Button.gone() {
    this.visibility = View.GONE
}