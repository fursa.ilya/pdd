package org.fursa.dev.pdd.ui.details

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.details_fragment.*
import org.fursa.dev.pdd.MainActivity
import org.fursa.dev.pdd.R
import org.fursa.dev.pdd.data.common.AppConst.TICKET_NUMBER_KEY
import org.fursa.dev.pdd.data.eventbus.HideEvent
import org.greenrobot.eventbus.EventBus

class DetailsFragment : DialogFragment() {
    private lateinit var viewModel: DetailsViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).setSupportActionBar(detailsToolbar)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(DetailsViewModel::class.java)
        EventBus.getDefault().post(HideEvent(true))

        detailsToolbar.setNavigationOnClickListener {
            findNavController().navigate(R.id.fragment_tickets)
            EventBus.getDefault().post(HideEvent(false))
            dismiss()
        }

        val ticketNumber = arguments?.getInt(TICKET_NUMBER_KEY)
        detailsToolbar.title = "${resources.getString(R.string.ticket_title)} $ticketNumber"
        Toast.makeText(context, "Ticket number: $ticketNumber", Toast.LENGTH_LONG).show()

        buttonStart.setOnClickListener {
            val args = bundleOf(TICKET_NUMBER_KEY to ticketNumber)
            findNavController().navigate(R.id.quiz_fragment, args)
        }

        buttonResults.setOnClickListener {

        }
    }

}
