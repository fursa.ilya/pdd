package org.fursa.dev.pdd.data.common

object AppConst {
    const val TICKET_NUMBER_KEY = "ticket_number"
    const val QUESTION_HEADER = "question"
    const val VARIANT_A = "a"
    const val VARIANT_B = "b"
    const val VARIANT_C = "c"
    const val VARIANT_D = "d"
    const val VARIANT_CORRECT = "correct"
    const val IMG = "img"
    const val TIP_HEADER = "tip_header"
    const val TIP_BODY = "tip_body"
}