package org.fursa.dev.pdd.ui.quiz

data class Attempt(
    val questionNumber: Int,
    val answer: String,
    val expected: String,
    val correct: Boolean
) {
    override fun toString(): String {
        return "Attempt(" +
                "questionNumber=$questionNumber," +
                " answer='$answer', " +
                "expected='$expected'," +
                "correct=$correct)"
    }
}