package org.fursa.dev.pdd.ui.quiz

import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.quiz_fragment.*
import org.fursa.dev.pdd.MainActivity
import org.fursa.dev.pdd.R
import org.fursa.dev.pdd.data.common.AppConst.TICKET_NUMBER_KEY
import org.fursa.dev.pdd.data.eventbus.HideEvent
import org.fursa.dev.pdd.extensions.gone
import org.fursa.dev.pdd.extensions.loadImg
import org.fursa.dev.pdd.extensions.visible
import org.fursa.dev.pdd.ui.dialogs.TipBottomDialog
import org.greenrobot.eventbus.EventBus

class QuizFragment : DialogFragment() {
    private lateinit var viewModel: QuizViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel = ViewModelProviders.of(this).get(QuizViewModel::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.quiz_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).setSupportActionBar(quizToolbar)
        viewModel.loadTicket(context!!.applicationContext, arguments!!.getInt(TICKET_NUMBER_KEY))
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.quiz_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
       if(item.itemId == R.id.action_help) {
           val dialog = TipBottomDialog.getInstance(
               title = viewModel.tipHeader.value.toString(),
               tip = viewModel.tipValue.value.toString())
           dialog.show(activity!!.supportFragmentManager, dialog.tag)
       }
       return true
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        quizToolbar.setNavigationOnClickListener { showExitDialog() }

        viewModel.getNextQuestion().observe(this, Observer {

            txtQuestion.text = it.question

            if(it.imageUrl!!.isEmpty()) {
                image.gone()
                image.setImageResource(R.drawable.placeholder)
                Toast.makeText(context, "Empty url", Toast.LENGTH_LONG).show()
            } else {
                image.visible()
                image.loadImg(it.imageUrl)
                Toast.makeText(context, "Url ${it.imageUrl}", Toast.LENGTH_LONG).show()
            }
            buttonA.text = it.a
            buttonB.text = it.b
            buttonC.text = it.c
            buttonD.text = it.d

            if(buttonD.text.isEmpty()) {
                buttonD.gone()
            } else {
                buttonD.visible()
            }

            viewModel.tipValue.value = it.tipBody
            viewModel.tipHeader.value = it.tipHeader


            buttonA.setOnClickListener {
                viewModel.submitAnswer(buttonA.text.toString())
            }

            buttonB.setOnClickListener {
                viewModel.submitAnswer(buttonB.text.toString())
            }

            buttonC.setOnClickListener {
                viewModel.submitAnswer(buttonC.text.toString())
            }

            buttonD.setOnClickListener {
                viewModel.submitAnswer(buttonD.text.toString())
            }

            if(viewModel.questionCount == 19) {
                Toast.makeText(context, "Correct answer count is ${viewModel.correctAnswersCount}", Toast.LENGTH_LONG).show()
                dismiss()
            }
        })


}

    private fun showExitDialog() {
        MaterialAlertDialogBuilder(context)
            .setTitle(R.string.exit_dialog_title)
            .setMessage(R.string.exit_dialog_message)
            .setPositiveButton(android.R.string.ok) { dialog, which ->
                dismiss()
                findNavController().navigate(R.id.fragment_tickets)
                EventBus.getDefault().post(HideEvent(false))
            }
            .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
            .show()
    }


}
