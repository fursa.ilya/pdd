package org.fursa.dev.pdd.ui.ticket

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.tickets_fragment.*
import org.fursa.dev.pdd.R

class TicketsFragment : Fragment() {
    private lateinit var viewModel: TicketsViewModel
    private val ticketAdapter = TicketAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.tickets_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(ticketsList) {
            layoutManager = GridLayoutManager(context, 2)
            adapter = ticketAdapter
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(TicketsViewModel::class.java)
        viewModel.getTickets().observe(this,
            Observer { ticketAdapter.setTickets(tickets = it.toMutableList()) }
        )

    }

}
