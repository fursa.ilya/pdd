package org.fursa.dev.pdd.data.common

data class Question(
    val question: String,
    val a: String,
    val b: String,
    val c: String,
    val d: String?,
    val tipHeader: String,
    val tipBody: String,
    val correct: String,
    val imageUrl: String?
) {
    override fun toString(): String {
        return "Question(question='$question')"
    }
}