package org.fursa.dev.pdd.data.common

import android.content.Context
import org.json.JSONObject

fun fileToString(filename: String, context: Context): String {
    return context.applicationContext.assets
        .open(filename)
        .bufferedReader()
        .use { it.readText() }
}

fun fetchQuestions(context: Context, ticketNumber: Int): MutableList<Question> {
    val result = mutableListOf<Question>()
    val jsonQuiz = fileToString(
        "ticket_${ticketNumber}.json",
        context
    )
    val jsonObj = JSONObject(jsonQuiz)
    val quizArr = jsonObj.getJSONArray("questions")

    for (i in 0..19)
    {
        val quiz = quizArr.getJSONObject(i)
        val title = quiz.getString(AppConst.QUESTION_HEADER)
        val a = quiz.getString(AppConst.VARIANT_A)
        val b = quiz.getString(AppConst.VARIANT_B)
        val c = quiz.getString(AppConst.VARIANT_C)
        val d = quiz?.getString(AppConst.VARIANT_D)
        val tipHeader = quiz.getString(AppConst.TIP_HEADER)
        val tipBody = quiz.getString(AppConst.TIP_BODY)
        val imageUrl = quiz?.getString(AppConst.IMG)
        val correct = quiz.getString(AppConst.VARIANT_CORRECT)

        val question = Question(
            question = title,
            a = a,
            b = b,
            c = c,
            d = d,
            tipHeader = tipHeader,
            tipBody = tipBody,
            correct = correct,
            imageUrl = imageUrl
        )

        result.add(question)
    }

    return result
}
