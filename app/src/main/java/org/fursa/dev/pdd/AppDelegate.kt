package org.fursa.dev.pdd

import android.app.Application
import org.fursa.dev.pdd.di.AppComponent
import org.fursa.dev.pdd.di.DaggerAppComponent
import org.fursa.dev.pdd.di.DatabaseModule


class AppDelegate : Application() {

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent
            .builder()
            .databaseModule(DatabaseModule(this))
            .build()
        appComponent.inject(this)
    }

    companion object {
        lateinit var appComponent: AppComponent
    }
}