package org.fursa.dev.pdd.ui.ticket

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TicketsViewModel : ViewModel() {
    private var ticketsLive = MutableLiveData<List<Ticket>>()
    private var mTicketList = mutableListOf<Ticket>()

    init {
        fillTickets()
    }

    private fun fillTickets() {
        for (i in 1..20 step 1) {
            mTicketList.add(Ticket(i, 0))
        }

        ticketsLive.value = mTicketList
    }

    fun getTickets() = ticketsLive

    override fun onCleared() {
        super.onCleared()
    }

}
