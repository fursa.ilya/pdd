package org.fursa.dev.pdd.ui.quiz

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import org.fursa.dev.pdd.AppDelegate
import org.fursa.dev.pdd.data.common.Question
import org.fursa.dev.pdd.data.common.fetchQuestions

class QuizViewModel(app: Application) : AndroidViewModel(app) {
    private lateinit var mQuestions: List<Question>
    private var currentQuestion = MutableLiveData<Question>()
    private var answerValue = MutableLiveData<Boolean>()

    var correctAnswersCount = 0

    var tipValue = MutableLiveData<String>()
    var tipHeader = MutableLiveData<String>()

    var questionCount = 0

    init {
        AppDelegate.appComponent.inject(this)
    }

    fun loadTicket(context: Context, ticketNumber: Int) {
        mQuestions =
            fetchQuestions(context, ticketNumber)
    }

    fun getNextQuestion(): MutableLiveData<Question> {
        val question = mQuestions[questionCount]
        currentQuestion.value = question
        Log.d("QuizViewModel", mQuestions.size.toString())
        Log.d("QuizViewModel", "Image url: ${question.imageUrl}")
        return currentQuestion
    }

    fun submitAnswer(answer: String) {
        val question = mQuestions[questionCount]
        questionCount++
        answerValue.value = answer == question.correct
        Log.d("QuizViewModel", "Submit: $answer Wait: ${question.correct} Result: ${answerValue.value}")
        Log.d("QuizViewModel", "Counter: $questionCount")

        if(answerValue.value == true) {
            correctAnswersCount++
            Log.d("Quiz", "Correct count: $correctAnswersCount")
        }

    }

    fun getAnswer() = answerValue


    override fun onCleared() {
        super.onCleared()
    }


}
